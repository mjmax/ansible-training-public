sudo yum install epel-release -y && \
sudo yum install vim ansible lsof sshpass wget -y

ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -P ""

ssh-keyscan -H 10.10.10.20 >> ~/.ssh/known_hosts
ssh-keyscan -H 10.10.10.30 >> ~/.ssh/known_hosts

export PASSWORD="vagrant"
/bin/sshpass -p "$PASSWORD" ssh-copy-id 10.10.10.20
/bin/sshpass -p "$PASSWORD" ssh-copy-id 10.10.10.30
